package maps;

import java.util.ArrayList;
import java.util.Collection;

import clases.Borders;
import clases.EnemyTanks;
import clases.Walls;
import clases.Water;

/**
 * Map generator for the first level
 * 
 * Freedom_tankers Map1.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         23 abr. 2020
 */
public class Map1 {

	/**
	 * creates the limits of the map and the non-destructible
	 * 
	 * @param terrain
	 * @return
	 */
	public static Collection<? extends Borders> terrain(ArrayList<Borders> terrain2) {
		// limit of the map
		Borders left = new Borders("limit1", 0, 0, 30, 1020, "images/enviroment/limit.png");
		Borders right = new Borders("limit2", 1700, 0, 1730, 1020, "images/enviroment/limit.png");
		Borders up = new Borders("limit3", 30, 0, 1730, 30, "images/enviroment/limitup.png");
		Borders down = new Borders("limit4", 30, 990, 1730, 1020, "images/enviroment/limitup.png");
		terrain2.add(up); // add ceiling
		terrain2.add(down); // add ground
		terrain2.add(left); // add left wall
		terrain2.add(right); // add right wall
		// player side
		Borders midup = new Borders("midup", 810, 30, 850, 440, "images/enviroment/mwmiddle.png");
		Borders middown = new Borders("middown", 810, 550, 850, 990, "images/enviroment/mwmiddle.png");
		Borders mmidup = new Borders("mmidup", 250, 210, 650, 250, "images/enviroment/mwmiddlemid.png");
		Borders mmiddown = new Borders("mmiddown", 250, 760, 650, 800, "images/enviroment/mwmiddlemid.png");
		Borders mmid = new Borders("mmid", 670, 310, 710, 670, "images/enviroment/mwmiddle.png");
		terrain2.add(midup); // add middle up
		terrain2.add(middown); // add middle down
		terrain2.add(mmidup); // add middle up middle
		terrain2.add(mmiddown); // add middle down middle
		terrain2.add(mmid); // add middle up
		// enemy side
		Borders midup2 = new Borders("middown", 1420, 310, 1460, 670, "images/enviroment/mwmiddle.png");
		Borders mmid2 = new Borders("midup", 990, 210, 1380, 250, "images/enviroment/mwmiddlemid.png");
		Borders middown2 = new Borders("midup", 990, 760, 1380, 800, "images/enviroment/mwmiddlemid.png");
		terrain2.add(mmid2); // add middle up middle
		terrain2.add(middown2); // add middle down middle
		terrain2.add(midup2); // add middle down
		return terrain2;

	}

	/**
	 * creates the water elements of the map
	 * 
	 * @param river
	 * @return
	 */
	public static Collection<? extends Water> river(ArrayList<Water> river) {
		// middle river
		Water water = new Water("water", 280, 312, 400, 697, 0, "images/enviroment/longriver.gif"); // left
		Water water2 = new Water("water", 1200, 250, 1379, 760, 0, "images/enviroment/longriver.gif"); // right
		river.add(water); // add water
		river.add(water2); // add water
		// small rivers
		Water water3 = new Water("water", 1480, 30, 1700, 210, 0, "images/enviroment/river.gif"); // upper
		Water water4 = new Water("water", 1480, 800, 1700, 990, 0, "images/enviroment/river.gif"); // bottom
		river.add(water3); // add water
		river.add(water4); // add water
		// side rivers
		Water water5 = new Water("water", 150, 30, 810, 130, 0, "images/enviroment/longerriver.gif"); // upper
		Water water6 = new Water("water", 150, 890, 810, 990, 0, "images/enviroment/longerriver.gif"); // bottom
		river.add(water5); // add water
		river.add(water6); // add water
		return river;
	}

	/**
	 * creates the brick walls that can destroyed
	 * 
	 * @param walls
	 * @return
	 */
	public static Collection<? extends Walls> walls(ArrayList<Walls> walls2) {
		// every brick wall is 40 x 40
		int x1 = 1000;
		int y1 = 500;
		for (int i = 0; i < 10; i++) {
			Walls wall = new Walls("wall", x1, 80, x1 + 40, 120, "images/enviroment/commonwall.png");
			walls2.add(wall);
			Walls wall2 = new Walls("wall", x1, 120, x1 + 40, 160, "images/enviroment/commonwall.png");
			walls2.add(wall2);
			Walls wall3 = new Walls("wall", x1, 855, x1 + 40, 895, "images/enviroment/commonwall.png");
			walls2.add(wall3);
			Walls wall4 = new Walls("wall", x1, 895, x1 + 40, 935, "images/enviroment/commonwall.png");
			walls2.add(wall4);
			x1 += 40;
		}
		y1 = 310;
		for (int i = 0; i < 9; i++) {
			Walls wall = new Walls("wall", 480, y1, 520, y1 + 40, "images/enviroment/commonwall.png");
			Walls wall2 = new Walls("wall", 520, y1, 560, y1 + 40, "images/enviroment/commonwall.png");
			Walls wall3 = new Walls("wall", 1000, y1, 1040, y1 + 40, "images/enviroment/commonwall.png");
			Walls wall4 = new Walls("wall", 1040, y1, 1080, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall);
			walls2.add(wall2);
			walls2.add(wall3);
			walls2.add(wall4);
			y1 += 40;
		}
		// symbols walls
		int x1p = 30;
		int x1e = 1540;
		for (int i = 0; i < 4; i++) {
			Walls basewall1 = new Walls("wall", x1p, 350, x1p + 40, 390, "images/enviroment/commonwall.png");
			walls2.add(basewall1);
			Walls basewall2 = new Walls("wall", x1p, 540, x1p + 40, 580, "images/enviroment/commonwall.png");
			walls2.add(basewall2);
			Walls basewall3 = new Walls("wall", x1e, 350, x1e + 40, 390, "images/enviroment/commonwall.png");
			walls2.add(basewall3);
			Walls basewall4 = new Walls("wall", x1e, 540, x1e + 40, 580, "images/enviroment/commonwall.png");
			walls2.add(basewall4);
			x1p += 40;
			x1e += 40;
		}
		int y1p = 380;
		int y1e = 390;
		for (int i = 0; i < 4; i++) {
			Walls basewall1 = new Walls("wall", 150, y1p, 190, y1p + 40, "images/enviroment/commonwall.png");
			walls2.add(basewall1);
			Walls basewall2 = new Walls("wall", 1540, y1e, 1580, y1e + 40, "images/enviroment/commonwall.png");
			walls2.add(basewall2);
			y1p += 40;
			y1e += 40;
		}
		return walls2;
	}

	/**
	 * Creates the enemies of this map
	 * 
	 * @param enemies
	 * @return
	 */
	public static Collection<? extends EnemyTanks> enemies(ArrayList<EnemyTanks> enemies) {

		EnemyTanks enemy = new EnemyTanks("enemy", 890, 40, 950, 100, 180, "images/tanks/bad_tank.png", 30, 100, 5);
		EnemyTanks enemy2 = new EnemyTanks("enemy2", 890, 920, 950, 980, 0, "images/tanks/bad_tank.png", 30, 100, 5);
		EnemyTanks enemy3 = new EnemyTanks("enemy3", 1630, 670, 1690, 730, 270, "images/tanks/bad_tank.png", 30, 100,
				5);
		EnemyTanks enemy4 = new EnemyTanks("enemy4", 1130, 700, 1190, 760, 270, "images/tanks/bad_tank.png", 30, 100,
				5);
		EnemyTanks enemy5 = new EnemyTanks("enemy3", 1630, 230, 1690, 290, 270, "images/tanks/bad_tank.png", 30, 100,
				5);
		EnemyTanks enemy6 = new EnemyTanks("enemy4", 1130, 250, 1190, 310, 270, "images/tanks/bad_tank.png", 30, 100,
				5);
		enemies.add(enemy);
		enemies.add(enemy2);
		enemies.add(enemy3);
		enemies.add(enemy4);
		enemies.add(enemy5);
		enemies.add(enemy6);
		return enemies;
	}

}

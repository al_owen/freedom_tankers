package maps;

import java.util.ArrayList;
import java.util.Collection;

import clases.Borders;
import clases.EnemyTanks;
import clases.Walls;
import clases.Water;

/**
 * Map generator for the second level
 * 
 * Freedom_tankers Map2.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         24 abr. 2020
 */
public class Map2 {

	/**
	 * creates the limits of the map and the non-destructible
	 * 
	 * @param terrain
	 * @return
	 */
	public static Collection<? extends Borders> terrain(ArrayList<Borders> terrain2) {
		Borders left = new Borders("limit1", 0, 0, 30, 1020, "images/enviroment/limit.png");
		Borders right = new Borders("limit2", 1700, 0, 1730, 1020, "images/enviroment/limit.png");
		Borders up = new Borders("limit3", 30, 0, 1730, 30, "images/enviroment/limitup.png");
		Borders down = new Borders("limit4", 30, 990, 1730, 1020, "images/enviroment/limitup.png");
		terrain2.add(up); // add ceiling
		terrain2.add(down); // add ground
		terrain2.add(left); // add left wall
		terrain2.add(right); // add right wall
		// metalwall
		Borders midleft = new Borders("midleft", 280, 30, 320, 400, "images/enviroment/mwmiddle.png");
		Borders midright = new Borders("midright", 1430, 30, 1470, 400, "images/enviroment/mwmiddle.png");
		Borders midup = new Borders("midup", 670, 280, 1070, 320, "images/enviroment/mwmiddlemid.png");
		Borders middown = new Borders("middown", 670, 700, 1070, 740, "images/enviroment/mwmiddlemid.png");
		terrain2.add(midleft);
		terrain2.add(midright);
		terrain2.add(midup);
		terrain2.add(middown);
		return terrain2;
	}

	/**
	 * creates the water elements of the map
	 * 
	 * @param river
	 * @return
	 */
	public static Collection<? extends Water> river(ArrayList<Water> river) {
		// middle river
		Water water1 = new Water("water", 150, 400, 450, 600, 0, "images/enviroment/river.gif");
		Water water2 = new Water("water", 1300, 400, 1600, 600, 0, "images/enviroment/river.gif");
		river.add(water1); // add water
		river.add(water2); // add water
		return river;
	}

	/**
	 * creates the brick walls that can destroyed
	 * 
	 * @param walls
	 * @return
	 */
	public static Collection<? extends Walls> walls(ArrayList<Walls> walls2) {
		// every brick wall is 40 x 40

		int y1 = 610;

		// these are the bottom bricks
		for (int i = 0; i < 9; i++) {
			Walls wall1 = new Walls("wall", 220, y1, 260, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall1);
			Walls wall2 = new Walls("wall", 260, y1, 300, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall2);
			Walls wall3 = new Walls("wall", 300, y1, 340, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall3);
			Walls wall4 = new Walls("wall", 1390, y1, 1430, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall4);
			Walls wall5 = new Walls("wall", 1430, y1, 1470, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall5);
			Walls wall6 = new Walls("wall", 1470, y1, 1510, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall6);
			y1 += 40;
		}

		// these are the middle bricks top-down
		y1 = 360;
		for (int i = 0; i < 7; i++) {
			Walls wall1 = new Walls("wall", 720, y1, 760, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall1);
			Walls wall2 = new Walls("wall", 760, y1, 800, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall2);
			Walls wall3 = new Walls("wall", 920, y1, 960, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall3);
			Walls wall4 = new Walls("wall", 960, y1, 1000, y1 + 40, "images/enviroment/commonwall.png");
			walls2.add(wall4);
			y1 += 40;
		}

		// these are the middle bricks left-right
		int x1 = 800;
		for (int i = 0; i < 3; i++) {
			Walls wall1 = new Walls("wall", x1, 360, x1 + 40, 400, "images/enviroment/commonwall.png");
			Walls wall2 = new Walls("wall", x1, 400, x1 + 40, 440, "images/enviroment/commonwall.png");
			Walls wall3 = new Walls("wall", x1, 560, x1 + 40, 600, "images/enviroment/commonwall.png");
			Walls wall4 = new Walls("wall", x1, 600, x1 + 40, 640, "images/enviroment/commonwall.png");
			walls2.add(wall1);
			walls2.add(wall2);
			walls2.add(wall3);
			walls2.add(wall4);
			x1 += 40;
		}
		return walls2;
	}

	/**
	 * Creates the enemies of this map
	 * 
	 * @param enemies
	 * @return
	 */
	public static Collection<? extends EnemyTanks> enemies(ArrayList<EnemyTanks> enemies) {
		EnemyTanks enemy1 = new EnemyTanks("enemy", 430, 900, 490, 960, 0, "images/tanks/bad_tank.png", 15, 100, 5);
		EnemyTanks enemy2 = new EnemyTanks("enemy2", 430, 50, 490, 110, 180, "images/tanks/bad_tank.png", 15, 100, 5);
		EnemyTanks enemy3 = new EnemyTanks("enemy3", 1270, 900, 1330, 960, 0, "images/tanks/bad_tank.png", 15, 100, 5);
		EnemyTanks enemy4 = new EnemyTanks("enemy4", 1270, 50, 1330, 110, 180, "images/tanks/bad_tank.png", 15, 100, 5);
		enemies.add(enemy1);
		enemies.add(enemy2);
		enemies.add(enemy3);
		enemies.add(enemy4);
		return enemies;
	}

}

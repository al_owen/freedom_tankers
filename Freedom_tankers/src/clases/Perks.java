package clases;

import java.util.Random;

import Core.Sprite;

public class Perks extends Sprite {

	Random r = new Random();

	/**
	 * Constructor for the perk class
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 */
	public Perks(String name, int x1, int y1, int x2, int y2, String path) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = false;
	}

	/**
	 * applies a perk to the character randomly it can be: more speed, more damage
	 * or heal to 100%
	 */
	public void collision() {
		if (this.collidesWith(Character.instance)) {
			this.delete();
			int randomizer = r.nextInt(3) + 1;
			System.out.println("random number " + randomizer);
			if (randomizer == 1) {
				Character.instance.speed++;
				Character.instance.lastSpeed++;
				Character.instance.bspeed++;
				System.out.println("speed " + Character.instance.speed);
			} else if (randomizer == 2) {
				Character.instance.attack += 10;
				System.out.println("attack " + Character.instance.attack);
			} else if (randomizer == 3) {
				Character.instance.hp = 100;
			} else {
				System.out.println("You got nothing");
			}
		}
	}

}

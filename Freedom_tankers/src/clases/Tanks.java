package clases;

import Core.Sprite;
import interfaces.Movement;

public abstract class Tanks extends Sprite implements Movement{

	// speed at which the tank moves
	public int speed;

	// speed at which the bullets moves
	int bspeed;

	// saves the last speed in case the speed is modified
	protected int lastSpeed;

	int cooldown = 25;

	// health points of the tank
	public int hp;

	// damage points that the tank deals
	public int attack;

	/**
	 * Constructor of the class tank
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param path   asks for the path to the image to that Sprite
	 * @param angle  The rotating angle in degrees
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the player's tank
	 * @param speed  sets the speed at which the tank will move
	 */
	public Tanks(String name, int x1, int y1, int x2, int y2, double angle, String path, int attack, int hp,
			int speed) {
		super(name, x1, y1, x2, y2, angle, path);
		this.attack = attack;
		this.hp = hp;
		this.speed = speed;
		this.lastSpeed = speed;
		this.bspeed = speed + 2;
		this.physicBody = true;
	}

	public void update() {
		if (cooldown < 30)
			cooldown++;
	}

	/**
	 * checks if a tanks has collided with the grass and reduces the speed accordingly
	 * 
	 * @param grass
	 */
	public abstract void forest(Forest grass);

}

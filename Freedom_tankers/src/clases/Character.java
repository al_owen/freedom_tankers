package clases;

import interfaces.Destructible;
import interfaces.Movement;
import interfaces.shoot;

public class Character extends Tanks implements shoot, Movement, Destructible {

	// creates a singleton character
	public static Character instance = null;

	/**
	 * private constructor for the main character
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 */
	private Character(String name, int x1, int y1, int x2, int y2, double angle, String path, int attack, int hp,
			int speed) {
		super(name, x1, y1, x2, y2, angle, path, attack, hp, speed);
		this.physicBody = true;
	}

	/**
	 * method for the creation of a Singleton character
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 * @return instance of the character
	 */
	public static Character getInstance(String name, int x1, int y1, int x2, int y2, double angle, String path,
			int attack, int hp, int speed) {
		if (instance == null) {
			instance = new Character(name, x1, y1, x2, y2, angle, path, attack, hp, speed);
		}
		return instance;
	}

	@Override
	public void forest(Forest grass) {
		if (this.collidesWith(grass)) {
			this.speed = this.lastSpeed / 2;
		} else {
			this.speed = this.lastSpeed;
		}

	}

	@Override
	public void destroy() {
//		if (hp > 0) {
//			hp--;
//		}else {
//			this.delete();
//		}
	}

	@Override
	public String toString() {
		return "Character [speed=" + speed + ", bspeed=" + bspeed + ", lastSpeed=" + lastSpeed + ", cooldown="
				+ cooldown + ", hp=" + hp + ", attack=" + attack + ", name=" + name + ", x1=" + x1 + ", y1=" + y1
				+ ", x2=" + x2 + ", y2=" + y2 + ", path=" + path + "]";
	}
}

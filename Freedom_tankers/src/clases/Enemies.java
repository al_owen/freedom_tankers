package clases;

/**
 * Freedom_tankers Enemies.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         20 abr. 2020
 */
public abstract class Enemies extends Tanks {

	/**
	 * Default constructor for the abstract enemy class
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 */
	public Enemies(String name, int x1, int y1, int x2, int y2, double angle, String path, int attack, int hp,
			int speed) {
		super(name, x1, y1, x2, y2, angle, path, attack, hp, speed);
	}

	/**
	 * implements how the enemy attacks
	 */
	public abstract void attack();

	// public abstract void move();

}

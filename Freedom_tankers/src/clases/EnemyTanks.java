package clases;

import java.awt.Color;
import java.util.ArrayList;

import Core.Sprite;
import game.MainGame;
import game.MainGame2;
import interfaces.Destructible;
import interfaces.shoot;

public class EnemyTanks extends Enemies implements shoot, Destructible {

	/**
	 * Constructor of the EnemyTanks class
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param angle  The rotating angle in degrees
	 * @param path   asks for the path to the image to that Sprite
	 * @param attack sets the damage that the tank deals in every shot
	 * @param hp     sets the health of the tank
	 * @param speed  sets the speed at which the tank will move
	 */
	public EnemyTanks(String name, int x1, int y1, int x2, int y2, double angle, String path, int attack, int hp,
			int speed) {
		super(name, x1, y1, x2, y2, angle, path, attack, hp, speed);
		this.physicBody = true;
	}

	@Override
	public void forest(Forest grass) {
		if (this.collidesWith(grass)) {
			this.speed = this.lastSpeed - 2;
		} else {
			this.speed = this.lastSpeed;
		}

	}

	@Override
	public void destroy() {
		if (this.hp > 0) {
			this.hp -= Character.instance.attack;
		} else {
			this.path = "images/bullets/kaboom.gif";
			this.delete();
		}

	}

	/*
	 * Still in progress, do not use yet.
	 */
	public void move(Symbols eagle, Sprite sp) {
		if (this.collidesWith(Character.instance)) {
			this.delete();
			Character.instance.hp -= this.attack;
		} else {
			if (this.x2 <= Character.instance.x2 && this.x1 <= Character.instance.x2
					|| this.x1 <= Character.instance.x1 && this.x2 >= Character.instance.x1) {
				if (this.y2 + 80 <= Character.instance.y1) {
					this.movedown(this);
				} else if (this.y1 - 80 >= Character.instance.y2) {
					this.moveup(this);
				}
			} else if (this.y1 <= Character.instance.y1 && this.y2 >= Character.instance.y1
					|| this.y1 <= Character.instance.y2 && this.y2 >= Character.instance.y2) {
				if (this.x1 - 50 < Character.instance.x2) {
					this.moveleft(this);
				} else if (this.x2 + 50 >= Character.instance.x1) {
					this.moveleft(this);

				}
			}

		}
	}

	@Override
	public String toString() {
		return "EnemyTanks [speed=" + speed + ", hp=" + hp + ", attack=" + attack + ", x1=" + x1 + ", y1=" + y1
				+ ", x2=" + x2 + ", y2=" + y2 + "]";
	}

	// public void move(Symbols eagle) {
//		//symbol
//		int middlex = (eagle.x1+eagle.x2)/2;
//		int middley = (eagle.y1+eagle.y2)/2;
//		//raycast
//		int rayx = (x1+x2)/2;
//		int rayy = (y1+y2)/2;
//		
//		if (!close(eagle, 'x') && middlex < rayx) {
//			if (!obstacle(eagle, x1, y1, x2, y2)) {
//				this.setVelocity(-3, 0);
//			}else {
//				int counter= 0;
//				while (obstacle(eagle, x1, y1-counter, x2, y2-counter) && obstacle(eagle, x1, y1+counter, x2, y2+counter)) {
//					counter++;
//					System.out.println(counter);
//				}
//				if (!obstacle(eagle, x1, y1-counter, x2, y2-counter)) {
//					this.setVelocity(0, -3);
//				}else {
//					this.setVelocity(0, 3);
//				}
//			}
//		}else if (!close(eagle, 'x') && middlex > rayx && !obstacle(eagle, x1, y1, x2, y2)) {
//			this.setVelocity(3, 0);
//		}else if (!close(eagle, 'y') && middley > rayy) {
//			this.setVelocity(0, 3);
//		}else if (!close(eagle, 'y') && middlex < rayy) {
//			this.setVelocity(0, -3);
//		}
//		//this.attack();
//	}

	public boolean close(Symbols eagle, char c) {
		int margin = 6;
		// symbol
		int middlex = (eagle.x1 + eagle.x2) / 2;
		int middley = (eagle.y1 + eagle.y2) / 2;
		// raycast
		int rayx = (x1 + x2) / 2;
		int rayy = (y1 + y2) / 2;
		if (c == 'x') {
			// out of margin
			if (middlex < rayx - margin || middlex > rayx + margin) {
				return false;
			} else {
				return true;
			}
		} else if (c == 'y') {
			if (middley < rayy - margin || middley > rayy + margin) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	private boolean obstacle(Symbols eagle, int rayx1, int rayy1, int rayx2, int rayy2) {
		// symbol
		int middlex = (eagle.x1 + eagle.x2) / 2;
		int middley = (eagle.y1 + eagle.y2) / 2;
		// raycast
		int rayx = (rayx1 + rayx2) / 2;
		int rayy = (rayy1 + rayy2) / 2;

		if (eagle.x2 < this.x1) {
			// is left
			Sprite rc = new Sprite("raycast", eagle.x2 + 2, rayy - ((rayy2 - rayy1) / 2) - 2, rayx1 - 2,
					rayy + ((rayy2 - rayy1) / 2) + 2, 0, "");
			if (rc.collidesWithField(MainGame.battleground).isEmpty()) {
				return false;
			} else {
				return true;
			}
		} else if (eagle.x1 > this.x2) {
			// is right
			Sprite rc = new Sprite("raycast", rayx2 + 2, rayy - 2, eagle.x1 - 2, rayy + 2, 0, "");
			if (rc.collidesWithField(MainGame.battleground).isEmpty()) {
				return false;
			} else {
				return true;
			}
		} else if (middley > rayy) {
			// bottom
			this.setVelocity(0, 3);
		} else if (middley < rayy) {
			// up
			this.setVelocity(0, -3);
		}

		return false;
	}

	@Override
	public void attack() {

	}

}

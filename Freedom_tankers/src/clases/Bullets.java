package clases;

import java.util.ArrayList;

import Core.Sprite;
import interfaces.Destructible;

public class Bullets extends Sprite{

	// bullets special variables
	public int bspeed;
	boolean friendly;

	/**
	 * This is the Bullet class, it creates bullets that deals damage and can be
	 * fired by tanks
	 * 
	 * @param name     sets the name of the sprite
	 * @param x1       The horizontal position in the field of the upper-left pixel
	 *                 in the sprite
	 * @param y1       The vertical position in the field of the upper-left pixel in
	 *                 the sprite
	 * @param x2       The horizontal position in the field of the bottom-right
	 *                 pixel in the sprite
	 * @param y2       The vertical position in the field of the bottom-right pixel
	 *                 in the sprite
	 * @param angle    The rotating angle in degrees
	 * @param path     asks for the path to the image to that Sprite
	 * @param friendly checks if the bullet was fired by the player or by an enemy
	 * @param bspeed   sets the speed at which the bullets will move
	 */
	public Bullets(String name, int x1, int y1, int x2, int y2, double angle, String path, boolean friendly,
			int bspeed) {
		super(name, x1, y1, x2, y2, angle, path);
		this.physicBody = true;
		this.trigger = true;
		this.friendly = friendly;
		this.bspeed = bspeed;
	}

	/**
	 * checks if the bullet collides with something and acts accordingly
	 * 
	 * @param sprites
	 */
	public void collision(ArrayList<Sprite> sprites) {
		for (Sprite sp : sprites) {
			if (sp instanceof Destructible) {
				if (this.collidesWith(sp) && !(sp instanceof Character)) {
					((Destructible) sp).destroy();
					this.delete();
				}
			}
		}
	}


}

package clases;

import Core.Sprite;
import enums.Status;
import interfaces.Destructible;

/**
 * Freedom_tankers Symbols.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         3 abr. 2020
 */
public class Symbols extends Sprite implements Destructible {

	int hp;
	private Status status;

	/**
	 * 
	 * @param name   sets the name of the sprite
	 * @param x1     The horizontal position in the field of the upper-left pixel in
	 *               the sprite
	 * @param y1     The vertical position in the field of the upper-left pixel in
	 *               the sprite
	 * @param x2     The horizontal position in the field of the bottom-right pixel
	 *               in the sprite
	 * @param y2     The vertical position in the field of the bottom-right pixel in
	 *               the sprite
	 * @param path   asks for the path to the image to that Sprite
	 * @param hp     sets the health of the tank
	 * @param status ends the game if the symbol dies
	 */
	public Symbols(String name, int x1, int y1, int x2, int y2, String path, int hp, Status status) {
		super(name, x1, y1, x2, y2, path);
		this.physicBody = true;
		this.hp = hp;
		this.setStatus(status);
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public void destroy() {
		if (this.hp > 0) {
			this.hp-=Character.instance.attack;
		}else {
			this.delete();
			setStatus(Status.DEAD);
		}
		
		
	}

}

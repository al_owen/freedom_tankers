package clases;

import Core.Sprite;

/**
 * 
 * Freedom_tankers
 * Water.java
 * @author Alvaro Owen de la Quintana
 *
 * 24 abr. 2020
 */
public class Water extends Sprite {
	/**
	 * Constructor for the water class
	 * 
	 * @param name sets the name of the sprite
	 * @param x1   The horizontal position in the field of the upper-left pixel in
	 *             the sprite
	 * @param y1   The vertical position in the field of the upper-left pixel in the
	 *             sprite
	 * @param x2   The horizontal position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param y2   The vertical position in the field of the bottom-right pixel in
	 *             the sprite
	 * @param path asks for the path to the image to that Sprite
	 */
	public Water(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		this.physicBody = true;
	}

}

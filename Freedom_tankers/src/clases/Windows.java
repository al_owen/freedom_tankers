package clases;

import Core.Field;
import Core.Window;

/**
 * 
 * Freedom_tankers Windows.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         24 abr. 2020
 */
public class Windows {

	// Window variable
	public static Window w = null;

	/**
	 * Constructor of window
	 * 
	 * @param f
	 */
	private Windows(Field f) {
		w = new Window(f);
	}

	/**
	 * Method for the creation of a Window
	 * 
	 * @param field
	 * @return
	 */
	public static Window getWindow(Field f) {
		if (w == null) {
			new Windows(f);
		} else {
			w.changeField(f);
		}
		return w;
	}

}

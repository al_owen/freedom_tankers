package interfaces;

import clases.Bullets;
import clases.Tanks;

public interface shoot {

	/**
	 * function that allows a tank to shoot up
	 * 
	 * @param tank
	 * @return
	 */
	public default Bullets shootUp(Tanks p) {
		Bullets s = new Bullets("bang", (p.x1 + p.x2) / 2 - 25, (p.y1 + p.y2) / 2 - 5, (p.x1 + p.x2) / 2 + 25,
				(p.y1 + p.y2) / 2 + 5, 270, "images/bullets/bullets.png", true, 9);
		s.setVelocity(0, -s.bspeed);
		return s;
	}

	/**
	 * function that allows a tank to shoot right
	 * 
	 * @param tank
	 * @return
	 */
	public default Bullets shootRight(Tanks p) {
		Bullets s = new Bullets("bang", (p.x1 + p.x2) / 2 - 25, (p.y1 + p.y2) / 2 - 5, (p.x1 + p.x2) / 2 + 25,
				(p.y1 + p.y2) / 2 + 5, 0, "images/bullets/bullets.png", true, 9);
		s.setVelocity(s.bspeed, 0);
		return s;
	}

	/**
	 * function that allows a tank to shoot down
	 * 
	 * @param tank
	 * @return
	 */
	public default Bullets shootDown(Tanks p) {
		Bullets s = new Bullets("bang", (p.x1 + p.x2) / 2 - 25, (p.y1 + p.y2) / 2 - 5, (p.x1 + p.x2) / 2 + 25,
				(p.y1 + p.y2) / 2 + 5, 90, "images/bullets/bullets.png", true, 9);
		s.setVelocity(0, s.bspeed);
		return s;
	}

	/**
	 * function that allows a tank to shoot left
	 * 
	 * @param tank
	 * @return
	 */
	public default Bullets shootLeft(Tanks p) {
		Bullets s = new Bullets("bang", (p.x1 + p.x2) / 2 - 25, (p.y1 + p.y2) / 2 - 5, (p.x1 + p.x2) / 2 + 25,
				(p.y1 + p.y2) / 2 + 5, 180, "images/bullets/bullets.png", true, 9);
		s.setVelocity(-s.bspeed, 0);
		return s;
	}
}

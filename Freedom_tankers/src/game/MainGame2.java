package game;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Sprite;
import Core.Window;
import clases.Borders;
import clases.Bullets;
import clases.Character;
import clases.EnemyTanks;
import clases.Forest;
import clases.Perks;
import clases.Symbols;
import clases.Text;
import clases.Walls;
import clases.Water;
import clases.Windows;
import enums.Facing;
import enums.Status;
import maps.Map2;

/**
 * 
 * Freedom_tankers MainGame.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         21 ene. 2020
 */
public class MainGame2 {

	// timer for control purposes
	static Timer timer = new Timer();
	static Random r = new Random();

	// variable to control whether we can fire or not
	static boolean cooldownFire = false;

	// Checks whether there's an active perk or not *yet to be implemented*
	static boolean activePerk = false;

	// Variable to know where the tank is facing
	static Facing facing = Facing.UP;

	// Battleground of the game
	static Field battleground2 = new Field();

	// Window where the game takes place
	static Window w = Windows.getWindow(battleground2);

	// Creates the main tank of the player
	static Character player;

	// creates the Symbols that tanks must protect
	static Symbols eagle = new Symbols("eagle", 830, 470, 890, 530, "images/Symbols/eagle.png", 200, Status.ALIVE);

	// list of water elements
	static ArrayList<Water> water = new ArrayList<Water>();

	// list of the limits of the map
	static ArrayList<Borders> terrain = new ArrayList<Borders>();

	// list of the brick walls
	static ArrayList<Walls> walls = new ArrayList<Walls>();

	// list of perks
	static ArrayList<Perks> perks = new ArrayList<Perks>();

	// list of shot bullets
	static ArrayList<Bullets> shot = new ArrayList<Bullets>();

	// list of enemies
	static ArrayList<EnemyTanks> enemies = new ArrayList<EnemyTanks>();

	// ************************* The game itself *********************************
	public static void main(String[] args) throws InterruptedException {

		// text that displays on screen showing the player's stats
		int color = 0xffffff;
		Text stats = new Text("stats", 1740, 50, 1900, 100, "Your character's stats:");
		Text stathp = new Text("stats", 1740, 100, 1900, 150, "Health: ");
		Text statsp = new Text("stats", 1740, 150, 1900, 200, "Speed: ");
		Text statdp = new Text("stats", 1740, 200, 1900, 250, "Damage: ");
		stats.textColor = color;
		stathp.textColor = color;
		statsp.textColor = color;
		statdp.textColor = color;

		// Instantiation of the player tank
		player = Character.getInstance("char", 90, 910, 150, 970, 0, "images/tanks/amerikan.gif", 25, 100, 7);

		battleground2.setBackground(Color.DARK_GRAY); // set background color

		boolean GameOver = false; // condition to keep playing

		// middle forest
		Forest grass = new Forest("grass", 590, 320, 1140, 700, "images/enviroment/grassx.png");

		// map setup
		enemies.addAll(Map2.enemies(enemies));
		terrain.addAll(Map2.terrain(terrain));
		water.addAll(Map2.river(water));
		walls.addAll(Map2.walls(walls));

		// start of the timers
		perk();
		firecooldown();

		w.playMusic("sounds/backgroundsound.wav"); // background music

		while (!GameOver) {

			player.update();
			ArrayList<Sprite> sprites = new ArrayList<Sprite>(); // sprites list
			sprites.addAll(terrain); // adds terrain
			sprites.add(grass); // adds trees
			sprites.addAll(water); // adds water
			sprites.addAll(walls); // adds walls
			sprites.add(eagle); // adds base symbol
			sprites.addAll(perks); // adds perks
			sprites.addAll(shot); // adds bullet
			sprites.addAll(enemies); // adds enemy
			sprites.add(player); // adds player character

			// modify the player's stats if there are changes
			sprites.add(stats);
			stathp.path = "Health: " + player.hp;
			statsp.path = "Speed: " + player.speed;
			statdp.path = "Damage: " + player.attack;
			sprites.add(stathp);
			sprites.add(statsp);
			sprites.add(statdp);

			// enemyatack(sprites); // *do not use yet, work still in progress*

			Ongrass(grass);
			input();
			hit(sprites);
			perkcol();

			// System.out.println(player); //info of the player

			battleground2.draw(sprites); // draw map
			Thread.sleep(30);

			GameOver = finish(GameOver);

		}
		if (GameOver) { // check if the game keeps going
			w.playSFX("sounds/GameOver.wav");
			w.stopMusic();
			w.close();
		}
	}
	// *****************************************************************************

	/**
	 * check if the tank collides with the perk and applies it
	 */
	private static void perk() {
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				int lucky = r.nextInt(6);
				if (lucky == 2 && !activePerk) {
					Perks perk = new Perks("perk", 110, 70, 170, 130, "images/miscellaneous/perk.png");
					perks.add(perk);
					activePerk = true;
				} else if (lucky == 4 && !activePerk) {
					Perks perk = new Perks("perk", 1550, 70, 1610, 130, "images/miscellaneous/perk.png");
					perks.add(perk);
					activePerk = true;
				}
			}
		};
		timer.schedule(task, 0, 60000);
	}

	/**
	 * cooldown for the bullets
	 */
	private static void firecooldown() {

		TimerTask cooldown = new TimerTask() {

			@Override
			public void run() {
				if (!cooldownFire) {
					cooldownFire = true;
					// System.out.println(cooldownFire);
				}
			}
		};
		timer.schedule(cooldown, 0, 1000);

	}

	/**
	 * checks if the bullet collides with something
	 * 
	 * @param sprites
	 */
	private static void hit(ArrayList<Sprite> sprites) {
		for (Bullets bullets : shot) {
			bullets.collision(sprites);
		}
	}

	/**
	 * checks whether the player is stepping on grass
	 * 
	 * @param grass
	 */
	private static void Ongrass(Forest grass) {
		player.forest(grass);
	}
	
	/**
	 * check if the player collided with a perk
	 */
	private static void perkcol() {
		for (Perks rperks : perks) {
			rperks.collision();
		}
	}

	/**
	 * 
	 * @param sprites
	 */
//	private static void enemyatack(ArrayList<Sprite> sprites) {
//
//		for (EnemyTanks t : enemies) {
//			t.atack();
//			// System.out.println(t);
//		}
//	}

	
	/**
	 * checks the game is over or not
	 * 
	 * @param gameOver
	 * @return
	 */
	private static boolean finish(boolean gameOver) {
		if (eagle.getStatus() == Status.DEAD) {
			gameOver = true;
		} else {
			gameOver = false;
		}
		return gameOver;
	}

	/**
	 * checks which key is being pressed
	 * 
	 * @throws InterruptedException
	 */
	private static void input() throws InterruptedException {

		if (!w.getPressedKeys().contains('w') && !w.getPressedKeys().contains('W') && !w.getPressedKeys().contains('d')
				&& !w.getPressedKeys().contains('D') && !w.getPressedKeys().contains('s')
				&& !w.getPressedKeys().contains('S') && !w.getPressedKeys().contains('a')
				&& !w.getPressedKeys().contains('A')) {
			player.stopmoving(player);
		} else {
			if (w.getPressedKeys().contains('d') || w.getPressedKeys().contains('D')) {
				player.moveright(player);
				facing = Facing.RIGHT;
			}
			if (w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W')) {
				player.moveup(player);
				facing = Facing.UP;
			}
			if (w.getPressedKeys().contains('a') || w.getPressedKeys().contains('A')) {
				player.moveleft(player);
				facing = Facing.LEFT;
			}
			if (w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S')) {
				player.movedown(player);
				facing = Facing.DOWN;
			}
		}

		fire();

	}

	/**
	 * shoots the bullet
	 * 
	 * @throws InterruptedException
	 */
	private static void fire() throws InterruptedException {

		if (cooldownFire) {
			if (facing == Facing.UP && w.getKeysDown().contains(' ')) {
				w.playSFX("sounds/shot.wav");
				shot.add(player.shootUp(player));
				cooldownFire = false;
			} else if (facing == Facing.RIGHT && w.getKeysDown().contains(' ')) {
				w.playSFX("sounds/shot.wav");
				shot.add(player.shootRight(player));
				cooldownFire = false;
			} else if (facing == Facing.DOWN && w.getKeysDown().contains(' ')) {
				w.playSFX("sounds/shot.wav");
				shot.add(player.shootDown(player));
				cooldownFire = false;
			} else if (facing == Facing.LEFT && w.getKeysDown().contains(' ')) {
				w.playSFX("sounds/shot.wav");
				shot.add(player.shootLeft(player));
				cooldownFire = false;
			}

		}
	}
}

 package game;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import Core.Field;
import Core.Sprite;
import Core.Window;
import clases.Text;
import clases.Windows;

/**
 * 
 * Freedom_tankers MainMenu.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         23 mar. 2020
 */
public class MainMenu {

	static Timer timer = new Timer();
	static boolean KeyCooldown = false;

	static boolean play = false; // condition to keep playing

	static int level = 1; // sets which level will the player play

	static Field f = new Field();
	static Window w = Windows.getWindow(f);
	static Sprite tank = new Sprite("char", 600, 500, 660, 560, 90, "images/tanks/yellow.png");

	/**
	 * @param args
	 * @throws InterruptedException
	 */
	public static void main(String[] args) throws InterruptedException {

		Sprite level1 = new Sprite("level1", 700, 500, 1100, 560, "images/menu/level1.png");
		Sprite level2 = new Sprite("level2", 700, 600, 1100, 660, "images/menu/level2.png");
		Sprite quit = new Sprite("quit", 700, 700, 1100, 760, "images/menu/exit.png");
		
		int color = 0xffffff;
		Text info = new Text("stats", 1300, 900, 1900, 980,
				"Move with 'w' (up) and 's' (down) and press 'e' to select an option");
		info.textColor = color;

		f.background = "images/menu/menuBackground.png"; // Background for the Main menu
		w.playMusicOnce("sounds/intro.wav"); // original song from Battle city
		cooldown();

		while (!play) {

			ArrayList<Sprite> sprites = new ArrayList<Sprite>();
			sprites.add(tank); // add character
			sprites.add(level1); // add level1
			sprites.add(level2); // add level2
			sprites.add(quit); // exit
			sprites.add(info); // shows relevant info
			input();

			f.draw(sprites); // draw map
			Thread.sleep(30); // 30fps
		}

		if (play) {
			if (level == 1) {
				MainGame.main(null); // if selected opens level 1
			} else if (level == 2) {
				MainGame2.main(null); // if selected opens level 2
			} else { // otherwise closes the window and stops the music
				w.stopMusic();
				w.playSFX("sounds/GameOver.wav");
				w.close();
			}
		}

	}

	/**
	 * cooldown for the keys
	 */
	private static void cooldown() {

		TimerTask cooldown = new TimerTask() {

			@Override
			public void run() {
				if (!KeyCooldown) {
					KeyCooldown = true;
				}
			}
		};
		timer.schedule(cooldown, 0, 600);

	}

	/**
	 * input for the menu
	 * 
	 * @throws InterruptedException
	 */
	private static void input() throws InterruptedException {
		// checks whether we can press the button again or not
		if (KeyCooldown) {
			if (w.getPressedKeys().contains('w') || w.getPressedKeys().contains('W')) { // goes up
				if (tank.y1 == 600 && tank.y2 == 660) {
					tank.y1 = 500;
					tank.y2 = 560;
					level = 1;
					KeyCooldown = false;
				} else if (tank.y1 == 700 && tank.y2 == 760) {
					tank.y1 = 600;
					tank.y2 = 660;
					level = 2;
					KeyCooldown = false;
				}
			}
			if (w.getPressedKeys().contains('s') || w.getPressedKeys().contains('S')) { // goes down
				if (tank.y1 == 500 && tank.y2 == 560) {
					tank.y1 = 600;
					tank.y2 = 660;
					level = 2;
					KeyCooldown = false;
				} else if (tank.y1 == 600 && tank.y2 == 660) {
					tank.y1 = 700;
					tank.y2 = 760;
					level = 3;
					KeyCooldown = false;
				}

			}
			if (w.getPressedKeys().contains('e') || w.getPressedKeys().contains('E')) { // selects the option
				play = true;
			}
		}
	}

}

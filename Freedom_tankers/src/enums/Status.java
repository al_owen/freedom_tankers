package enums;

/**
 * Freedom_tankers Status.java
 * 
 * @author Alvaro Owen de la Quintana
 *
 *         16 abr. 2020
 */
public enum Status {

	DEAD,
	ALIVE
}
